**Loops\Exception**
================

Base class for exception with code and replacements.



Requirements:
-------------

- At least PHP 5.3.
- Loops\Autoloader package.



How to use it:
--------------


### Autoloading

If you do not have composer, call bootstrap:

    :::php
      require $path_to_package.'/bootstrap.inc.php';

*This library use package-oriented autoloading with a directory for package 
namespace (PSR-4) and underscore as directory separator for class names (PSR-0).*


### Extend

The `\Loops\Exception\Base` class has been designed to be extended in order to 
isolate an array of error codes for a specific exception.

    :::php
      class MyException extends \Loops\Exception\Base
      {
        public $_messages = array(
          'CODE1' => 'Message 1' ,
          'CODE2' => 'Message 2' ,
        );
      }


### Throw

Then, you should be able to throw an exception using a code, and the message
argument as an array for `sprintf()` replacements.

    :::php
      // create the exception class
      class MyException extends \Loops\Exception\Base
      {
        public $_messages = array(
          'ME101' => 'Invalid configuration' ,
          'ME102' => 'Invalid value "%s"' ,
          'ME103' => 'Invalid range "%02d;%02d"' ,
        );
      }
 
      // invalid configuration
      $e = new MyException( null , 'ME101' );
      $m = $e->getMessage(); // result to 'Invalid configuration'
 
      // invalid value
      $e = new MyException( 'foo' , 'ME102' );
      $m = $e->getMessage(); // result to 'Invalid value "foo"'
 
      // invalid range
      $e = new MyException( array( 0 , 42 ) , 'ME103' );
      $m = $e->getMessage(); // result to 'Invalid range "00;42"'


### Purpose

The two goals of this class are:

- manage all exception messages in a single place;
- force usage of a exception code for each exception messages.


### Trace Ø

A `getTrace0()` method is also available to fetch the line where the exception 
has been thrown. This method comes with the `getTrace0AsString()` one.



Contributors:
-------------

- Pierrot Evrard aka Loops — [https://twitter.com/lxxps](https://twitter.com/lxxps)


### Wanna contribute?

There is only one rule to follow: **Challenge yourself**.