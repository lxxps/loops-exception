**POOP: Permissive Oriented Object Programming**
=================================================


POOP (Permissive Oriented Object Programming) is a recommendation that advocates
these three practices:

1. Use `public` visibility
2. Simplify namespaces
3. Omit type hinting

- POOP is not dirty
- POOP is not useless
- POOP is not dangerous
- POOP is not hard

**POOP is not poop.**

---------------------------------------

## Use `public` visibility

In this package, every property or method prefixed by a single underscore "_" 
should be considered as `protected`: **do not use them if you do not know what 
you are doing**.

When bypass a property or call a `pseudo-protected` method, your code 
is **out of standard usage**. In other words, if you break the code by this way, 
it is not an issue and you have to correct it by yourself.

*PHP magic methods exception: Methods that starts with two underscores "__" are 
"magic" and can be used safely.*

---------------------------------------

## Simplify namespaces

This package follows a special package-oriented autoloading, that mix PSR-4 
(prefix and base directory for the package) and PSR-0 (underscore in class names 
as directory separator).

This package never use fully qualified name for global functions, so if you 
encounter issues with any of these functions, just make a function with the same
name available in the package namespace to overwrite it.

---------------------------------------

## Omit type hinting

In this package, not any method has type hinting on argument or return value. By
this way, we preserve dynamic ability of PHP language.


