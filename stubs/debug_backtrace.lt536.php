<?php

/*
 * This file is part of the loops/exception package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Exception;

/**
 * A stub for debug_backtrace() usage with PHP < 5.3.6
 */
if( version_compare( \PHP_VERSION , '5.3.6' ) < -1 )
{
  if( ! function_exists( __NAMESPACE__.'\\debug_backtrace' ) )
  {
    // below PHP 5.3.6, these constants should not exists
    define( '\\DEBUG_BACKTRACE_PROVIDE_OBJECT' , 1 );
    define( '\\DEBUG_BACKTRACE_IGNORE_ARGS' , 2 );
    
    /**
     * 
     * @see http://php.net/manual/en/function.debug-backtrace.php
     * @param [integer] $options
     * @param [integer] $limit
     */
    function debug_backtrace( $options = \DEBUG_BACKTRACE_PROVIDE_OBJECT , $limit = 0 )
    {
      // below PHP 5.3.6
      // - options was boolean for provide_object
      // - limit was missing
      $trace = \debug_backtrace( $options & \DEBUG_BACKTRACE_PROVIDE_OBJECT );
      
      if( $limit > 0 )
      {
        // reduce trace
        $trace = array_slice( $trace , 0 , $limit , false );
      }
      
      if( $options & \DEBUG_BACKTRACE_IGNORE_ARGS )
      {
        $i = count( $trace );
        while( $i-- )
        {
          unset( $trace[$i]['args'] );
        }
      }
      
      return $trace;
    }
    
  }
}
