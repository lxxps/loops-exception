<?php

/*
 * This file is part of the loops/exception package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Exception;

/**
 * Class to manage special exception where code is a key for a message with 
 * sprintf() replacements, and where message is an array of replacement to apply
 * on the message.
 * 
 * To use it efficiently, you should create a class that extends it with its 
 * own messages:
 * 
 * class MyException extends \Loops\Exception\Base
 * {
 *
 *   public $_messages = array(
 *    CODE1 => 'Message 1' ,
 *    CODE2 => 'Message 2' ,
 *   );
 * 
 * }
 *
 * @package    loops/exception
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Base extends \Exception
{
  /**
   * Array of exceptions messages by code.
   * Range of message are:
   *  - from X to Y: Group of exceptions
   * 
   * @var array
   * @access protected
   */
  public $_messages = array(
    // From X to Y: Group of exceptions
  );
  
  /**
   * The trace where exception is thrown
   * 
   * @var array
   * @access protected
   */
  public $_trace0;
  
  /**
   * \Loops\Exception\Base constructor.
   * 
   * Used with code attribute, message can be a replacement string
   * or an array of replacements used with sprintf.
   * 
   * If the code does not exists in messages proprety, just the string or
   * the first value of the array will be used.
   * 
   * If the last parameter is not an Exception (or Throwable since PHP 7), the 
   * parent constructor will probably rise a Fatal Error, so we do not need to
   * type it.
   *
   * @param [mixed] $message Message
   * @param [mixed] $code Code
   * @param [Exception] $previous Previous Exception (since PHP 5.3.0)
   * @access public
   */
  public function __construct( $message = null , $code = null , $previous = null )
  {
    
    if( $code !== null && isset($this->_messages[$code]) )
    {
      if( is_array( $message ) )
      {
        array_unshift( $message , $this->_messages[$code] );
        $message = call_user_func_array( 'sprintf' , $message );
      }
      else
      {
        $message = sprintf( $this->_messages[$code] , $message );
      }
    }
    elseif( is_array( $message ) )
    {
      $message = current($message);
    }
    
    // \Exception only accepts integer as code
    parent::__construct( $message , null , $previous );
    
    // but we can assign it after parent constructor :)
    $this->code = $code;
    
    // assign 0 backtrace
    list( $this->_trace0 ) = debug_backtrace( 0 , 1 );
  }
  
  /**
   * Get the original trace (subject 0)
   * 
   * @param none
   * @return string
   * @access public
   */
  public function getTrace0()
  {
    return $this->_trace0;
  }
  
  /**
   * Get the original trace as string (subject 0)
   * 
   * @param none
   * @return string
   * @access public
   */
  public function getTrace0AsString()
  {
    // make sure these keys are set
    $t = $this->_trace0 + array( 'class' => null , 'type' => null , 'args' => array() );
    // init replacement string
    $s = '#%d %s(%d): %s(%s)'; // no PHP_EOL
    
    // stringify arguments
    $a = '';
    for( $i = 0, $imax = count($t['args']); $i < $imax; $i++ )
    {
      switch( gettype( $t['args'][$i] ) )
      {
        case 'NULL':
          $a .= ', null';
        break;
        case 'boolean':
          $a .= ', '.( $t['args'][$i] ? 'true' : 'false' );
        break;
        case 'array':
          $a .= ', Array';
        break;
        case 'object':
          $a .= ', Object('.get_class( $t['args'][$i] ).')';
        break;
        case 'string':
          $a .= ', \''.$t['args'][$i].'\'';
        break;
        default:
          $a .= ', '.(string)$t['args'][$i];
      }
    }
    // remove first coma
    $a and ( $a = substr( $a , 2 ) );
    
    // this original backtrace is at index -1
    return sprintf( $s , -1 , $t['file'] , $t['line'] , $t['class'].$t['type'].$t['function'] , $a );
  }
  
  /**
   * Dump all messages available for this exception
   * 
   * @param none
   * @return array
   * @access public
   */
  public function dump()
  {
    return $this->_messages;
  }
}