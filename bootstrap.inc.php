<?php

/*
 * This file is part of the loops/exception package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Exception;

// composer should have required dependencies before to call this file

// add prefix and root for this package
\Loops\Autoloader\Psr40::getInstance()->add( __NAMESPACE__ , __DIR__.'/src' );