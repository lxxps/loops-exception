<?php

/*
 * This file is part of the loops/exception package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Exception;

use Loops\Exception\Base as ExceptionBase;

/**
 * Exceptions test suite for Loops\Exception\Base
 *
 * @package    loops/exception
 * @author     Loops <pierrotevrard@gmail.com>
 */
class BaseTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: message conversion
   */
  public function test1()
  {
    eval( 'namespace Loops\Exception;'
        . 'class MyException1 extends Base'
        . '{'
        . '  public $_messages = array('
        . '    "ME01A" => "Message 1A" ,'
        . '    "ME01B" => "Message 1B \\"%s\\"" ,'
        . '    "ME01C" => "Message 1C \\"%02d:%02d\\"" ,'
        . '  );'
        . '}' );
    
    // message without replacement
    $e = new MyException1( null , 'ME01A' );
    $this->assertSame( $e->getMessage() , 'Message 1A' );
    $this->assertSame( $e->getCode() , 'ME01A' );
    
    // message with one replacement
    $e = new MyException1( 'test' , 'ME01B' );
    $this->assertSame( $e->getMessage() , 'Message 1B "test"' );
    $this->assertSame( $e->getCode() , 'ME01B' );
    
    // message with two replacements
    $e = new MyException1( array( 0 , 42 ) , 'ME01C' );
    $this->assertSame( $e->getMessage() , 'Message 1C "00:42"' );
    $this->assertSame( $e->getCode() , 'ME01C' );
  }
  
  /*
   * Test case 2: trace0
   */
  public function test2()
  {
    eval( 'namespace Loops\Exception;'.PHP_EOL
        . 'class MyException2 extends Base'.PHP_EOL
        . '{'.PHP_EOL
        . '  public $_messages = array('.PHP_EOL
        . '    "ME02A" => "Message 2A" ,'.PHP_EOL
        . '    "ME02B" => "Message 2B \\"%s\\"" ,'.PHP_EOL
        . '    "ME02C" => "Message 2C \\"%02d:%02d\\"" ,'.PHP_EOL
        . '  );'.PHP_EOL
        . '}'.PHP_EOL
        . ''.PHP_EOL
        . 'class MyClass2'.PHP_EOL
        . '{'.PHP_EOL
        . '  public function failure1()'.PHP_EOL
        . '  {'.PHP_EOL
        . '    throw new MyException2( null , "ME02A" );'.PHP_EOL // line 15
        . '  }'.PHP_EOL
        . '  public function failure2()'.PHP_EOL
        . '  {'.PHP_EOL
        . '    throw new MyException2( "failure" , "ME02B" , new \RuntimeException() );'.PHP_EOL // line 19
        . '  }'.PHP_EOL
        . '  public function failure3()'.PHP_EOL
        . '  {'.PHP_EOL
        . '    throw new MyException2( array( 0 , 42 ) , "ME02C" , new MyException2( null , "ME02A" ) );'.PHP_EOL // line 23
        . '  }'.PHP_EOL
        . '}'.PHP_EOL
        . ''.PHP_EOL );
    
    // create instance
    $c = new MyClass2();
    
    // failure 1
    try
    {
      $c->failure1();
    }
    catch( MyException2 $e )
    {
      // test the trace0 string
      // #-1 path_to_file(15): Loops\Exception\Base->__construct(null, 'ME02A')
      // keep in mind that preg_match() return 1, not true
      $this->assertTrue( (bool)preg_match( '~^#-1 .*\\(15\\): Loops\\\\Exception\\\\Base->__construct\\(null, \'ME02A\'\\)$~' , $e->getTrace0AsString() ) );
    }
    
    // failure 2
    try
    {
      $c->failure2();
    }
    catch( MyException2 $e )
    {
      // test the trace0 string
      // #-1 path_to_file(19): Loops\Exception\Base->__construct('failure', 'ME02B', Object(RuntimeException))
      // keep in mind that preg_match() return 1, not true
      $this->assertTrue( (bool)preg_match( '~^#-1 .*\\(19\\): Loops\\\\Exception\\\\Base->__construct\\(\'failure\', \'ME02B\', Object\\(RuntimeException\\)\\)$~' , $e->getTrace0AsString() ) );
    }
    
    // failure 3
    try
    {
      $c->failure3();
    }
    catch( MyException2 $e )
    {
      // test the trace0 string
      // #-1 path_to_file(23): Loops\Exception\Base->__construct(Array, 'ME02C', Object(Loops\Exception\MyException2))
      // keep in mind that preg_match() return 1, not true
      $this->assertTrue( (bool)preg_match( '~^#-1 .*\\(23\\): Loops\\\\Exception\\\\Base->__construct\\(Array, \'ME02C\', Object\\(Loops\\\\Exception\\\\MyException2\\)\\)$~' , $e->getTrace0AsString() ) );
    }
    
  }
  
  
  /*
   * Test case 3: backward
   */
  public function test3()
  {
    // empty exception
    $e = new ExceptionBase();
    
    $this->assertSame( $e->getMessage() , '' );
    $this->assertSame( $e->getCode() , null );
    
    
    // undefined code
    $e = new Base( 'Message 3A' , 'ME03A');
    
    $this->assertSame( $e->getMessage() , 'Message 3A' );
    $this->assertSame( $e->getCode() , 'ME03A' );
    
    
    // array message to string
    $e = new Base( array( 'Message 3B' ) , 'ME03B' );
    
    $this->assertSame( $e->getMessage() , 'Message 3B' );
    $this->assertSame( $e->getCode() , 'ME03B' );
  }
}
